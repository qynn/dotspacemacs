(setq-default
;; Base distribution to use. This is a layer contained in the directory
;; `+distribution'. For now available distributions are `spacemacs-base'
;; or `spacemacs'. (default 'spacemacs)
dotspacemacs-distribution 'spacemacs
;; Lazy installation of layers (i.e. layers are installed only when a file
;; with a supported type is opened). Possible values are `all', `unused'
;; and `nil'. `unused' will lazy install only unused layers (i.e. layers
;; not listed in variable `dotspacemacs-configuration-layers'), `all' will
;; lazy install any layer that support lazy installation even the layers
;; listed in `dotspacemacs-configuration-layers'. `nil' disable the lazy
;; installation feature and you have to explicitly list a layer in the
;; variable `dotspacemacs-configuration-layers' to install it.
;; (default 'unused)
dotspacemacs-enable-lazy-installation 'unused
;; If non-nil then Spacemacs will ask for confirmation before installing
;; a layer lazily. (default t)
dotspacemacs-ask-for-lazy-installation t
;; If non-nil layers with lazy install support are lazy installed.
;; List of additional paths where to look for configuration layers.
;; Paths must have a trailing slash (i.e. `~/.mycontribs/')
dotspacemacs-configuration-layer-path '()
;; https://github.com/syl20bnr/spacemacs/issues/11142
sp-escape-quotes-after-insert nil
;; List of configuration layers to load.
dotspacemacs-configuration-layers

;;  _
;; | |__ _ _  _ ___ _ _ ___
;; | / _` | || / -_) '_(_-<
;; |_\__,_|\_, \___|_| /__/
;;        |__/
;; layers

'(
  (html :variables
         sgml-basic-offset 4
         css-enable-lsp t
         less-enable-lsp t
         scss-enable-lsp t
         html-enable-lsp t)

  yaml
  json
  prettier
  csv
  javascript
  (typescript :variables
              typescript-fmt-on-save nil
              typescript-fmt-tool 'prettier
              typescript-linter 'eslint
              typescript-backend 'lsp)
  ;; react
  ;; nginx
  ;; vimscript
  rust
  (go :variables go-backend 'lsp
                 go-format-before-save t)
  ruby

  (markdown :variables
            markdown-live-preview-engine 'vmd)

  ;;still having issues w/ dvipng when calling org-toggle-latex-fragment
  (org :variables
        org-enable-github-support t
        org-latex-create-formula-image-program 'dvipng)
        ;; org-latex-pdf-process 'dvipng)
        ;; org-enable-reveal-js-support t)

  (latex :variables
          latex-enable-magic t
          ;; latex-build-command "LateX"
          latex-enable-folding t)

  bibtex

  (ranger :variables
          ranger-show-preview t)

  (treemacs :variables
          treemacs-use-follow-mode t
          treemacs-project-follow-mode nil
          treemacs-use-filewatch-mode t
          treemacs-use-git-mode 'simple
          treemacs-collapse-dirs 2
          treemacs-lock-width t)

  (evil-snipe :variables
              evil-snipe-enable-alternate-f-and-t-behaviors t)

  (lsp :variables
       lsp-rust-server 'rust-analyzer)
  dap

  (python :variables
          python-backend 'lsp
          python-lsp-server 'pylsp
          python-test-runner 'pytest
          python-formatter 'yapf
          python-format-on-save nil)

  (debug :variables
          debug-additional-debuggers '("ipdb"))

  django

  (c-c++ :variables
          c-c++-enable-clang-support t
          c-c++=backend 'lsp-clangd
          c-c++=dap-adapters 'dap-cpptools)
          ;; c-c++=dap-adapters 'dap-gdb-lldb)
          ;; c-c++-default-mode-for-headers 'c-mode

  gtags

  emacs-lisp
  (elm :variables
        elm-backend 'lsp
        elm-format-on-save t
        elm-sort-imports-on-save t
  )
  ;; (ocaml :variables ocaml-format-on-save t)
  ;; (haskell :variables
  ;;           haskell-process-type 'stack-ghci)
  (clojure :variables clojure-enable-fancify-symbols t)

  ivy
  (auto-completion :variables
                    auto-completion-return-key-behavior 'complete
                    auto-completion-tab-key-behavior 'cycle
                    auto-completion-complete-with-key-sequence nil
                    auto-completion-complete-with-key-sequence-delay 0.1
                    auto-completion-enable-snippets-in-popup t
                    auto-completion-private-snippets-directory nil)
  spell-checking
  syntax-checking
  better-defaults
  (tree-sitter :variables
                tree-sitter-syntax-highlight-enable t
                spacemacs-tree-sitter-hl-black-list '(js2-mode rjsx-mode)
                tree-sitter-indent-enable t
                tree-sitter-fold-enable t
                tree-sitter-fold-indicators-enable nil)
  theming
  colors

  (shell :variables
          shell-default-shell `term
          shell-default-height 20
          shell-default-position 'bottom)

  (transmission :variables transmission-auto-refresh-all t)

  (git :variables git-enable-magit-delta-plugin t)

  (spacemacs-modeline :variables
                      version-control t)

  ;; mu4e
  ;; (org-bookmarks :variables
  ;;               org-bookmarks-dir "~/.bkmrks"
  ;;               )

  (gleam :variables
          gleam-enable-lsp t
          gleam-format-on-save t)

) ;;layers

;; List of additional packages that will be installed without being
;; wrapped in a layer. If you need some configuration for these
;; packages, then consider creating a layer. You can also put the
;; configuration in `dotspacemacs/user-config'.
dotspacemacs-additional-packages '(
  ;; ebuku
  ;; org-cliplink
  mw-thesaurus
  drag-stuff
)
;; A list of packages that cannot be updated.
dotspacemacs-frozen-packages '()
;; A list of packages that will not be installed and loaded.
dotspacemacs-excluded-packages '()
;; Defines the behaviour of Spacemacs when installing packages.
;; Possible values are `used-only', `used-but-keep-unused' and `all'.
;; `used-only' installs only explicitly used packages and uninstall any
;; unused packages as well as their unused dependencies.
;; `used-but-keep-unused' installs only the used packages but won't uninstall
;; them if they become unused. `all' installs *all* packages supported by
;; Spacemacs and never uninstall them. (default is `used-only')
dotspacemacs-install-packages 'used-only

);; matches (setq-default..
