;; ~/.emacs.d/private/local/qtils
(load "~/.emacs.d/private/local/qtils/ivy-jump.el")
;; (load "~/.emacs.d/private/local/bfs/bfs.el")

;;               __ _
;;  __ ___ _ _  / _(_)__ _
;; / _/ _ \ ' \|  _| / _` |
;; \__\___/_||_|_| |_\__, |
;;                   |___/
;; config

;; UNDO-TREE
(with-eval-after-load 'undo-tree
  (setq undo-tree-auto-save-history nil))

;; THESAURUS
;; API key set in .spacemacs.env
(setq mw-thesaurus--api-key (getenv "THESAURUS1"))
(spacemacs/set-leader-keys "sw" 'mw-thesaurus-lookup-at-point)

;; RANGER
(with-eval-after-load 'ranger
  (setq bookmarks-completion-ignore-case nil))
;; make sure dired buffer closes after entering directory
(setq ranger-cleanup-eagerly t)

;; Teensyduino uploader
(add-to-list 'exec-path (concat (getenv "HOME") "/Tnsy/teensy-loader-cli"))

;; Haskell
(add-to-list 'exec-path "~/.local/bin/")

;; SCSS
(setq scss-compile-at-save t)

;; M A I L (mu4e)
;; collecting mail
(setq mu4e-maildir (concat (getenv "HOME") "/Mail/riseup")
      user-mail-address "qynn@riseup.net"
      user-full-name  "Qynn"
      mu4e-compose-signature-auto-include nil
      mu4e-inbox-folder "/INBOX"
      mu4e-sent-folder "/SENT"
      mu4e-trash-folder "/trash"
      mu4e-spam-folder "/spam"
      mu4e-drafts-folder "/drafts"
      mu4e-refile-folder "/magique"
      mu4e-get-mail-command "mbsync -a"
      mu4e-update-interval 300
      mu4e-view-show-images t
      mu4e-view-show-addresses t
  )

  ;; sending mail
  (setq message-send-mail-function 'smtpmail-send-it
      smtpmail-stream-type 'starttls
      smtpmail-default-smtp-server "mail.riseup.net"
      smtpmail-smtp-server "mail.riseup.net"
      smtpmail-starttls-credentials '(("mail.riseup.net" 587 nil nil))
      smtpmail-auth-credentials '(("mail.riseup.net" 587 "qynn" nil))
      smtpmail-smtp-service 587)

  ;; ;; don't keep message buffers around
  (setq message-kill-buffer-on-exit t)


;; M O D E S
;; https://github.com/syl20bnr/spacemacs/issues/3919
;; https://emacs.stackexchange.com/questions/3397/how-to-replace-an-element-of-an-alist

;; associates .ino files w/ c++ minor mode
(add-to-list 'auto-mode-alist '("\\.ino\\'" . c++-mode))
(add-to-list 'auto-mode-alist '("\\.qml\\'" . c++-mode))
(spacemacs/set-leader-keys "omc" 'c++-mode)

;; global minor mode
(define-globalized-minor-mode global-rainbow-mode rainbow-delimiters-mode
  (lambda () (rainbow-delimiters-mode 1))
  )
(global-rainbow-mode 1)

;;  _ __  ___ _ _ _ _
;; | '_ \/ _ \ '_| ' \
;; | .__/\___/_| |_||_|
;; |_|
;; porn

;;SPC SPC custom-theme-visit-theme <enter> spolsky
;; see also (custom-set-faces
;; Watch out for Custom overriding values at the end of the file!!
;; (setq doom-modeline-buffer-modification-icon nil)

(setq theming-modifications '(
      (spolsky
        (default :background "#140414" :foreground "#ffddff")
        (hl-line :background "#500050")
        ;;NB "SPC SPC set-face-underline hl-line nil"
        (region :background "#ddbbff")
        (vertical-border :background "#140414" :foreground "#ffccee")
        (font-lock-function-name-face :weight normal :foreground "#cc99ff")
        (font-lock-constant-face :foreground "#992255")
        (font-lock-keyword-face :weight bold :foreground "#cc0060")
        (font-lock-type-face :weight normal :foreground "#ddbbff")
        (font-lock-builtin-face :foreground "#ff88bb")
        (fringe :background "#140414" :foreground "#ffccee")
        ;; really need to fix highlight background when using SPC-s-e !!
        (highlight :background "#e6db74" :foreground "#500050")
        (linum :background "#140414" :foreground "#cc99ff")
        (mode-line-inactive :background "#1f0010" :foreground "#ffccee")
        (mode-line :background "#cc99ff" :foreground "#0a0a0a")
        (lazy-highlight :background "#ff88bb" :foreground "#ffccee")
        (show-paren-match :background "#992255" :foreground "#ffccee")
        (cursor :background "#e6db74" :foreground:"#fff5f5")
        (spacemacs-insert-face :background "#cc0060" :foreground "#ffddff")
        (spacemacs-normal-face :background "#1f0010" :foreground "#ffddff")
        ;; TODO autocomplete menu
        (popup-face :background "#500050" :foreground "#ffddff")
        (popup-menu-face :background "#0000ff" :foreground "#00ff00")
        (popup-enu-selection-face :background "#00fff0" :foreground "#ff0000")
        )
))
(spacemacs/update-theme)

;;   __              _   _
;;  / _|_  _ _ _  __| |_(_)___ _ _  ___
;; |  _| || | ' \/ _|  _| / _ \ ' \(_-<
;; |_|  \_,_|_||_\__|\__|_\___/_||_/__/
;; functions

;; to save a macro permanently:
;; 1. perform the macro: q . . . q
;; 2. name the macro: SPC SPC kmacro-name-last-macro <name>
;; 3 insert the macro: SPC SPC insert-kbd-macro <RET> <name>

;; NB: to leave macro in insert mode: <C-o>-q

;; PYTHON
(defalias 'python-dbg-prt
  (kmacro "y i W o p r i n t ( \" <right> , SPC <escape> p F \" P ="))
(spacemacs/set-leader-keys-for-major-mode 'python-mode "p" 'python-dbg-prt)

;; JAVASCRIPT
(fset 'js-dbg-prt
  (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([121 105 87 111 99 111 110 115 111 108 101 46 108 111 103 40 right 59 left left escape 112 121 115 105 40 34 102 41 105 44 32 escape 112] 0 "%d")) arg)))
(spacemacs/set-leader-keys-for-major-mode 'js2-mode "p" 'js-dbg-prt)
(spacemacs/set-leader-keys-for-major-mode 'typescript-mode "d" 'js-dbg-prt)
(spacemacs/set-leader-keys "omj" 'js2-mode)

;; ELISP
(fset 'el-dbg-prt
  (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([73 40 112 114 105 110 116 right backspace 32 escape 65 41 escape] 0 "%d")) arg)))
(spacemacs/set-leader-keys-for-major-mode 'emacs-lisp-mode "p" 'el-dbg-prt)

;; shell
(fset 'sh-dbg-prt
  (kmacro-lambda-form [?- ?y ?t ?= ?o ?e ?c ?h ?o ?  ?\" escape ?p ?a ?  ?$ escape ?p] 0 "%d"))
(spacemacs/set-leader-keys-for-major-mode 'sh-mode "p" 'sh-dbg-prt)

;; MARKDOWN
(fset 'md-code-block
  (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([105 96 96 96 return escape 79 15] 0 "%d")) arg)))
(spacemacs/set-leader-keys-for-major-mode 'markdown-mode "`" 'md-code-block)

;; SPC o p = "2p
(fset 'paste-last-reg
  (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ("\"2p" 0 "%d")) arg)))
(spacemacs/set-leader-keys "op" 'paste-last-reg)
;; clean space inside parentheses (e.g. right after ysiw{)

;; cursor must be on first parenthesis
(fset 'clean-ys-space
  (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ("lxh%hx" 0 "%d")) arg)))
(spacemacs/set-leader-keys "oy" 'clean-ys-space)

;; devil macros (SPC-d)
;; TODO functionify these
(spacemacs/declare-prefix "d" "devil")
(fset 'bold-word (kmacro-lambda-form [?y ?s ?i ?w ?*] 0 "%d"))
(spacemacs/set-leader-keys "d*" 'bold-word)
(fset 'italic-word (kmacro-lambda-form [?y ?s ?i ?w ?/] 0 "%d"))
(spacemacs/set-leader-keys "d/" 'italic-word)
(fset 'quote-word (kmacro-lambda-form [?y ?s ?i ?w ?\"] 0 "%d"))
(spacemacs/set-leader-keys "d\"" 'quote-word)
(fset 'src-word (kmacro-lambda-form [?y ?s ?i ?w ?~] 0 "%d"))
(spacemacs/set-leader-keys "d`" 'src-word)

;; ORG
(with-eval-after-load 'org
  (setq-default tab-width 8)
  ;; Org-clock
  ;; S-up for 'org-timestamp-up
  ;; S-down for 'org-timestamp-down
  (setq org-time-stamp-rounding-minutes (quote (15 15)))
  ;; E: org-clock-in/out is still not rounding...
  (evil-define-key 'normal org-mode-map (kbd ",~") 'org-insert-structure-template)
  (setq org-duration-format (quote h:mm))
  ;; readmyorg
  (add-to-list 'org-structure-template-alist '("u" . "src user -r -l \"[%s]\""))
  (add-to-list 'org-structure-template-alist '("r" . "src root -r -l \"[%s]\""))
  (add-to-list 'org-structure-template-alist '("f" . "src file -r -l \"[%s]\""))
  ;; https://github.com/syl20bnr/spacemacs/issues/13465
  (setq org-src-tab-acts-natively nil)
  (setq org-todo-keywords
    (quote ((sequence "TODO" "CURR" "MAYBE" "|" "DONE" "DROP"))))

  (setq org-todo-keyword-faces
        (quote (("TODO" :foreground "#ff00ff" :weight bold)
                ("CURR" :foreground "#ffff00" :weight  bold)
                ("MAYBE" :foreground "#00ffff" :weight  bold)
                ("DROP" :foreground "#ff0000" :weight  bold)
                ("DONE" :foreground "#00ff00" :weight  bold))))
  )
  (add-hook 'org-mode-hook 'smartparens-mode)
  ;; tag completion
  (evil-define-key 'normal org-mode-map (kbd ",`") 'org-set-tags-command)

;; LATEX
(defun my-LaTeX-mode()
  (add-to-list 'TeX-view-program-list '("Okular" "okular --page=%(outpage) %o"))
  (add-to-list 'TeX-view-program-list '("Zathura" "zathura --page=%(outpage) %o"))
  (setq TeX-view-program-selection '((output-pdf "Zathura")))
  (setq Tex-engine 'xetex)
  ;;NB use SPC SPC Tex-engine-set xetex to use fontspec
)
(add-hook 'LaTeX-mode-hook 'my-LaTeX-mode)
(add-hook 'doc-view-mode-hook 'auto-revert-mode)

;; PYTHON
;; (setq python-shell-interpreter "python"
;;       python-shell-interpreter-args "-i $HOME/Gigs/drained/manage.py shell_plus")
(when (executable-find "ipython")
  (setq python-shell-interpreter "ipython"))
(add-hook 'python-mode-hook
  (lambda ()
    ;; evil-snipe config (use f~ to circulate through functions)
    (make-variable-buffer-local 'evil-snipe-aliases)
    (push '(?~ "def .+:") evil-snipe-aliases)
    ;; delete white space on saving
    (add-to-list 'write-file-functions 'delete-trailing-whitespace)
    ;; indents
    (setq-default indent-tabs-mode nil)
    (setq-default tab-width 4)
    (setq-default python-indent-offset 4)))

;; CPP
;; cpp is not python
(defun append-semicolon ()
  (interactive)
  (save-excursion
    (end-of-line)
    (insert ";")
    )
  )
(spacemacs/set-leader-keys-for-major-mode 'c++-mode ";" 'append-semicolon)

;; FILES
;;i3 config file
(defun i3-config ()
  (interactive)
  ;; simply using $HOME wouldn't work here
  (find-file-existing (concat (getenv "HOME") "/.qonf/i3/config")))
(spacemacs/set-leader-keys "fe3" 'i3-config)

;;orged qutebrowser config
(defun qute-config ()
  (interactive)
  (find-file-existing (concat (getenv "HOME") "/.qonf/qutebrowser/config.org")))
(spacemacs/set-leader-keys "feq" 'qute-config)

;; orged spacemacs config
(defun spacemacs-org-config ()
  (interactive)
  (find-file-existing (concat (getenv "HOME") "/.spacemacs.d/qonfig.org")))
(spacemacs/set-leader-keys "feo" 'spacemacs-org-config)

;;zsh cutom file
(defun zsh-divo ()
  (interactive)
  (find-file-existing (concat (getenv "HOME") "/.qonf/zsh/custom/divo/divo.zsh")))
(spacemacs/set-leader-keys "fez" 'zsh-divo)

;; MARKS
(defun update-marks ()
  "reset evil-visual-mark-mode"
  (evil-visual-mark-mode)
  (evil-visual-mark-mode))
(defun delete-mark (x)
  "delete mark and reset evil-visual-mark-mode"
  (interactive "sDelete mark: ")
  (evil-delete-marks x)
  (update-marks))
(defun delete-all-marks ()
  "delete all marks and reset evil-visual-mark-mode"
  (interactive)
  (evil-delete-marks "a" t)
  (update-marks))
(defun count-words-between-marks ()
  "count words between selected marks"
  (interactive)
  (let (b e wc cc)
    (setq b (evil-get-marker (string-to-char (read-string "start mark: "))))
    (setq e (evil-get-marker (string-to-char (read-string "end mark: "))))
    (setq wc (count-words b e))
    (setq cc (- e b))
    (message "%s->%s : %s words; %s chars"b e wc cc)
  )
)

;; W!: had to remove "'" from layers/+tools/shell/packages.el
;; (spacemacs/declare-prefix "'" "marks")
;; (spacemacs/set-leader-keys "'d" 'delete-mark)
;; (spacemacs/set-leader-keys "'D" 'delete-all-marks)
;; (spacemacs/set-leader-keys "'v" 'evil-visual-mark-mode)
;; (spacemacs/set-leader-keys "'c" 'count-words-between-marks)

;; LOG
;; open error log buffer
(defun show-messages ()
  (interactive)
  (switch-to-buffer-other-window "*Messages*")
  (goto-char (point-max))
  )
(spacemacs/set-leader-keys "oM" 'show-messages)

;; COMMENTS
(fset 'switch-comment
  (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ("2 cL" 0 "%d")) arg)))
(define-key evil-normal-state-map (kbd "#") 'switch-comment)

;; remap highlight-symbol-transient to toggle comments instead (same as SPC-c-L)
(define-key evil-normal-state-map (kbd "g;") 'spacemacs/comment-or-uncomment-lines-inverse)

;; SHELL
;; calls script called .spaceshell, if it exists in the current directory
(defun spaceshell ()
  (interactive)
  (shell-command "$PWD/.spaceshell"))
(spacemacs/set-leader-keys "ot" 'spaceshell)
(spacemacs/set-leader-keys "oT" 'spacemacs/default-pop-shell)

;;  _
;; | |_____ _  _ _ __  __ _ _ __ ___
;; | / / -_) || | '  \/ _` | '_ (_-<
;; |_\_\___|\_, |_|_|_\__,_| .__/__/
;;          |__/           |_|
;; keymaps

;; * * * B U F F E R / W I N D O W * * *
;;(SPC-b-d)
(spacemacs/set-leader-keys "K" 'spacemacs/kill-this-buffer)
;;(SPC-b-b)
(spacemacs/set-leader-keys "B" 'ivy-switch-buffer)
;;(SPC-f-s)
(spacemacs/set-leader-keys "RET" 'save-buffer)

;; (spacemacs/set-leader-keys "o0" 'mwim-beginning-of-code-or-line-or-comment)

;; swapping | and + to mirror i3's layout toggle
(spacemacs/set-leader-keys "w|" 'spacemacs/window-layout-toggle)
(spacemacs/set-leader-keys "w+" 'spacemacs/maximize-vertically)

;; * * * N A V I G A T I O N * * *

;; Num-s (-)
(define-key evil-normal-state-map (kbd "-") 'evil-first-non-blank)
(define-key evil-motion-state-map (kbd "-") 'evil-first-non-blank)
;; Num-S (_)
(define-key evil-normal-state-map (kbd "_") 'evil-beginning-of-line)
(define-key evil-motion-state-map (kbd "_") 'evil-beginning-of-line)
;; Num-d (=)
(define-key evil-normal-state-map (kbd "=") 'evil-end-of-line)
(define-key evil-motion-state-map (kbd "=") 'evil-end-of-line)
;; NB: Num-a (+) is already bound to evil-next-line-first-non-blank

;; jump accross screen; faster than SPC j j (is it?)
(spacemacs/declare-prefix "." "avy-goto-char-timer")
(spacemacs/set-leader-keys "." 'avy-goto-char-timer)

;; go to last change (g;)
(define-key evil-normal-state-map (kbd "g.") 'goto-last-change)

;; page navigation: swap {/}  and H/L
(define-key evil-normal-state-map (kbd "H") 'evil-backward-paragraph)
(define-key evil-motion-state-map (kbd "H") 'evil-backward-paragraph)
(define-key evil-normal-state-map (kbd "L") 'evil-forward-paragraph)
(define-key evil-motion-state-map (kbd "L") 'evil-forward-paragraph)
(define-key evil-normal-state-map (kbd "{") 'evil-window-top)
(define-key evil-motion-state-map (kbd "{") 'evil-window-top)
(define-key evil-normal-state-map (kbd "}") 'evil-window-bottom)
(define-key evil-motion-state-map (kbd "}") 'evil-window-bottom)

;; Alternative Scroll up (C-u) / down (C-d)
(define-key evil-normal-state-map (kbd "C-n") 'evil-scroll-down)
(define-key evil-normal-state-map (kbd "C-m") 'evil-scroll-up)
;; Alternative Scroll-line-up (C-y) / down (C-e)
(define-key evil-normal-state-map (kbd "M-n") 'evil-scroll-line-down)
(define-key evil-normal-state-map (kbd "M-m") 'evil-scroll-line-up)

;; Alternative evil-jump- backwards (C-o) / forward (C-i)
(define-key evil-normal-state-map (kbd "C-u") 'evil-jump-backward)
(define-key evil-normal-state-map (kbd "C-r") 'evil-jump-forward)
;; NB: evil scroll page up (C-b) / down (C-f)

;; char navigation in insert mode
;; Not useful when Num+hjkl is remapped to arrow keys system-wise
;; (define-key evil-insert-state-map (kbd "C-l") 'evil-forward-char)
;; (define-key evil-insert-state-map (kbd "C-h") 'evil-backward-char)
;; (define-key evil-insert-state-map (kbd "C-p") 'evil-paste-after)

;; * * * T A B / I N D E N T * * *
(setq tab-always-indent nil)
(setq python-indent-offset 4)
(define-key evil-insert-state-map (kbd "TAB") 'indent-for-tab-command)
;; NB: had to change tab-always-indent to nil
;; so that (indent-for-tab-command) in indent.el would behave appropriately
;; see SPC h d f indent-for-tab for more details

;; Num+,
(define-key evil-normal-state-map (kbd "~") 'evil-indent)
;; ~ was bound to evil-invert-char (not -case) and C-i to TAB
;; W! differentiating TAB and C-i in dotspacemacs/init()
;; with dotspacemacs-distinguish-gui-tab t' doesn't seem to work...
(define-key evil-normal-state-map (kbd "C-i") 'evil-invert-char)
;; NB C-i works in visual mode (useful in markdown tables)
(define-key evil-normal-state-map (kbd "TAB") 'evil-invert-case)
;; Org mode takes over TAB.. (see gu, gU, and g~)

;; * * * S E A R C H * * *
;; basic search and replace
(spacemacs/set-leader-keys "ss" 'replace-string)
;; search and replace w/ confirmation
(spacemacs/set-leader-keys "sq" 'query-replace)

;; using ripgrep instead of ag
(evil-leader/set-key "/" 'counsel-rg)
;; C-c C-o  to open search results into buffer (ivy occur)
;; C-x C-q  to toggle the read-only flag (wgrep mode)
;; C-c C-c  to commit the changes

;; W! search-project-rg-... only works on develop branch
(spacemacs/set-leader-keys "*" 'spacemacs/search-project-rg-region-or-symbol)

;; * * * S H O R T E R  C U T S * * *

(spacemacs/set-leader-keys
  "os" 'flyspell-correct-next
  "oS" 'flyspell-correct-previous
  "or" 'ranger ;;(no longer SPC-a-r on develop branch)
  "ow" 'delete-trailing-whitespace
  "of" 'describe-function
  "ok" 'describe-key
  "ov" 'describe-variable
  "oz" 'spacemacs/scale-font-transient-state/body)

;; remove underline on current line
(defun underline ()
  (interactive)
  (set-face-attribute hl-line-face nil :underline t)
)
(spacemacs/set-leader-keys "tU" 'underline)
(defun un-underline ()
  (interactive)
  (set-face-attribute hl-line-face nil :underline nil)
)
(spacemacs/set-leader-keys "tu" 'un-underline)
(add-hook 'after-init-hook 'un-underline)

(load-file "~/.qonf/ranger/bookmarks.el")
(spacemacs/set-leader-keys "oR" 'ranger-with-bookmarks)
;; (add-hook 'ranger-mode-load-hook 'parse-ranger-bookmarks-file)

;; * * * M I S C E L L A N E O U S * * *

;; RIP geany
;; https://github.com/syl20bnr/spacemacs/pull/14449
;; (define-key evil-normal-state-map (kbd "J") 'move-text-line-down)
(define-key evil-normal-state-map (kbd "J") 'drag-stuff-down)
;; (define-key evil-normal-state-map (kbd "K") 'move-text-line-up)
(define-key evil-normal-state-map (kbd "K") 'drag-stuff-up)
;; K was mapped to smart-doc
(spacemacs/set-leader-keys "od" 'evil-smart-doc-lookup)
;; J was mapped to evil-join, gJ to evil-join-whitespace
(define-key evil-normal-state-map (kbd "C-k") 'evil-join)
(define-key evil-normal-state-map (kbd "C-S-k") 'evil-join-whitespace)
;; C-k was doing essentially the same as D

;; see also [-SPC and ]-SPC to insert space above/below

;; map redo (Ctrl-r) on U
(define-key evil-normal-state-map (kbd "U") 'undo-redo)
;; remap highlight-symbol-transient to toggle comments instead (same as SPC-c-L)
(define-key evil-normal-state-map (kbd "g-;") 'spacemacs/comment-or-uncomment-lines-inverse)
