## spacemacs config ##

##### install spacemacs #####
```
$ sudo apt-get install emacs
$ git clone https://github.com/2ynn/spacemacs.git ~/.emacs.d
$ git clone https://gitlab.com/qynn/dotspacemacs.git ~/.spacemacs.d
```


##### install dependencies #####

0. magit

[git-delta](https://github.com/dandavison/delta/releases)

1. ripgrep (counsel-rg)

[ripgrep in emacs](https://gist.github.com/pesterhazy/fabd629fbb89a6cd3d3b92246ff29779)
```
$ sudo apt install ripgrep
```

2. vmd-mode (markdown preview)

[resolving EACCESS permissions](https://docs.npmjs.com/resolving-eacces-permissions-errors-when-installing-packages-globally)
```
$ mkdir ~/.npm-global
$ npm config set prefix '~/.npm-global'
$ npm install -g vmd
$ PATH=$PATH:$HOME/.npm-global/bin
```

3. GNU Global (gtags)
```
$ sudo apt install global
$ sudo apt install --reinstall ctags
$ cd ~/.emacs.d/.cache/ && gtags
```

5. LaTeX
```
$ sudo apt install --reinstall texlive-full
```

6. Python
```
$ python3 -m pip install yapf flake8 autoflake pytest ipdb python-lsp-server

```

7. Typescript
```
$ npm install -g typescript prettier typescript-language-server
```


##### launch spacemacs #####
```
$ emacs --debug-init
```

#### quirks ####
1. using [alternate dotspacemacs location](https://www.spacemacs.org/doc/DOCUMENTATION.html#orgheadline34) as git repository
2. `.spacemacs.env` is .gitignored
3. using tangled org file adapted from [github.com/ralesi/spacemacs.org](https://github.com/ralesi/spacemacs.org/blob/master/spacemacs.org). Elisp code is wrapped in code blocks like:
  ```
  #+BEGIN_SRC emacs-lisp :tangle user-init.el
  ;;
  #+END_SRC
  ```

  See [qonfig.org](./qonfig.org) for more details

  4. server is started automatically (see `emacs.d/init.el`):
  ```
  (setq-default dotspacemacs-enable-server t)
  ```
  5. might need to fetch the new [elpa keyring](https://elpa.gnu.org/packages/gnu-elpa-keyring-update.html):
    ```
    $ gpg --homedir ~/.emacs.d/elpa/.../gnupg --receive-keys 066DAFCB81E42C40
    ```
