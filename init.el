;; entangled spacemacs configuration using org mode (see qonfig.org)
;; adapted from https://github.com/ralesi/spacemacs.org/blob/master/spacemacs.org

(defun dotspacemacs/init ()
  "Initialization function.
This function is called at the very startup of Spacemacs initialization
before layers configuration.
You should not put any user code in there besides modifying the variable
values."
  ;; tangle without actually loading org
  (let ((src (concat dotspacemacs-directory "qonfig.org"))
        (qi (concat dotspacemacs-directory ".qonfig-init.el"))
        (ql (concat dotspacemacs-directory ".qonfig-layers.el"))
        (qui (concat dotspacemacs-directory ".qonfig-user-init.el"))
        (quc (concat dotspacemacs-directory ".qonfig-user-config.el")))
    (when (or (file-newer-than-file-p src qi)
              (file-newer-than-file-p src ql)
              (file-newer-than-file-p src qui)
              (file-newer-than-file-p src quc))
      (call-process
       (concat invocation-directory invocation-name)
       nil nil t
       "-q" "--batch" "--eval" "(require 'ob-tangle)"
       "--eval" (format "(org-babel-tangle-file \"%s\")" src)))
    (load-file qi)))

(defun dotspacemacs/layers ()
  "Configuration Layers declaration. You should not put any user code in this
function besides modifying the variable values."
  (let ((ql (concat dotspacemacs-directory ".qonfig-layers.el")))
    (message ql)
    (load-file ql)))

(defun dotspacemacs/user-init ()
  "Initialization for user code:
This function is called immediately after `dotspacemacs/init', before layer
configuration.
It is mostly for variables that should be set before packages are loaded.
If you are unsure, try setting them in `dotspacemacs/user-config' first."
  (let ((qui (concat dotspacemacs-directory ".qonfig-user-init.el")))
    (load-file qui)))

(defun dotspacemacs/user-config ()
  "Configuration for user code:
This function is called at the very end of Spacemacs startup, after layer
configuration.
Put your configuration code here, except for variables that should be set
before packages are loaded."
  (let ((quc (concat dotspacemacs-directory ".qonfig-user-config.el")))
    (message quc)
    (load-file quc)))

;; Do not write anything past this comment. This is where Emacs will
;; auto-generate custom variable definitions.
(defun dotspacemacs/emacs-custom-settings ()
  ;;   "Emacs custom settings.
  ;; This is an auto-generated function, do not modify its content directly, use
  ;; Emacs customize menu instead.
  ;; This function is called at the very end of Spacemacs initialization."
  ;; (custom-set-variables
  ;;  ;; custom-set-variables was added by Custom.
  ;;  ;; If you edit it by hand, you could mess it up, so be careful.
  ;;  ;; Your init file should contain only one such instance.
  ;;  ;; If there is more than one, they won't work right.
  ;;  '(ansi-color-names-vector
  ;;    ["#0a0814" "#f2241f" "#67b11d" "#b1951d" "#4f97d7" "#a31db1" "#28def0" "#b2b2b2"])
  ;;  '(evil-want-Y-yank-to-eol nil)
  ;;  '(hl-todo-keyword-faces
  ;;    '(("TODO" . "#dc752f")
  ;;      ("NEXT" . "#dc752f")
  ;;      ("THEM" . "#2d9574")
  ;;      ("PROG" . "#4f97d7")
  ;;      ("OKAY" . "#4f97d7")
  ;;      ("DONT" . "#f2241f")
  ;;      ("FAIL" . "#f2241f")
  ;;      ("DONE" . "#86dc2f")
  ;;      ("NOTE" . "#b1951d")
  ;;      ("KLUDGE" . "#b1951d")
  ;;      ("HACK" . "#b1951d")
  ;;      ("TEMP" . "#b1951d")
  ;;      ("FIXME" . "#dc752f")
  ;;      ("XXX+" . "#dc752f")
  ;;      ("\\?\\?\\?+" . "#dc752f")))
  ;;  '(linum-format " %7i ")
  ;;  '(package-selected-packages
  ;;    '(treemacs-all-the-icons tide cider sesman parseedn parseclj cider-eval-sexp-fu clojure-mode clojure-snippets cargo lsp-docker flycheck-rust ron-mode rust-mode toml-mode forge yaml magit ghub closql emacsql-sqlite emacsql treepy pydoc inspector hide-comnt git-modes gendoxy evil-terminal-cursor-changer elisp-def utop tuareg caml ocp-indent ocamlformat merlin-iedit merlin-eldoc merlin-company flycheck-ocaml merlin dune transmission phoenix-dark-pink-theme realgud test-simple loc-changes load-relative simpleclip dap-mode bui lsp-ui lsp-treemacs lsp-python-ms lsp-pyright lsp-origami origami lsp-latex lsp-ivy ccls tern yasnippet-snippets yapfify yaml-mode xterm-color ws-butler writeroom-mode winum which-key wgrep web-mode web-beautify vterm volatile-highlights vmd-mode vimrc-mode vi-tilde-fringe uuidgen use-package unfill undo-tree treemacs-projectile treemacs-persp treemacs-magit treemacs-icons-dired treemacs-evil toc-org terminal-here tagedit symon symbol-overlay sublime-themes string-inflection string-edit sphinx-doc spaceline-all-the-icons smex smeargle slim-mode shell-pop scss-mode sass-mode restart-emacs ranger rainbow-mode rainbow-identifiers rainbow-delimiters quickrun pytest pyenv-mode py-isort pug-mode prettier-js popwin pony-mode poetry pippel pipenv pip-requirements pcre2el password-generator paradox ox-gfm overseer orgit-forge org-superstar org-rich-yank org-projectile org-present org-pomodoro org-mime org-download org-cliplink org-brain open-junk-file npm-mode nose nodejs-repl nameless mwim mw-thesaurus multi-term multi-line mmm-mode markdown-toc magit-section magic-latex-buffer macrostep lsp-haskell lorem-ipsum livid-mode live-py-mode link-hint json-navigator json-mode js2-refactor js-doc jbeans-theme ivy-yasnippet ivy-xref ivy-rtags ivy-purpose ivy-hydra ivy-avy indent-guide importmagic impatient-mode hybrid-mode hungry-delete hlint-refactor hl-todo hindent highlight-parentheses highlight-numbers highlight-indentation helm-make haskell-snippets google-translate google-c-style golden-ratio gnuplot gitignore-templates gitignore-mode gitconfig-mode gitattributes-mode git-timemachine git-messenger git-link gh-md ggtags fuzzy font-lock+ flyspell-correct-ivy flycheck-ycmd flycheck-rtags flycheck-pos-tip flycheck-package flycheck-haskell flycheck-elsa flycheck-elm flx-ido fancy-battery eyebrowse expand-region evil-visualstar evil-visual-mark-mode evil-unimpaired evil-tutor evil-textobj-line evil-surround evil-snipe evil-org evil-numbers evil-nerd-commenter evil-matchit evil-lisp-state evil-lion evil-indent-plus evil-iedit-state evil-goggles evil-exchange evil-escape evil-ediff evil-easymotion evil-collection evil-cleverparens evil-args evil-anzu eval-sexp-fu eshell-z eshell-prompt-extras esh-help emr emmet-mode elm-test-runner elm-mode elisp-slime-nav editorconfig ebuku dumb-jump drag-stuff dotenv-mode disaster dired-quick-sort diminish devdocs define-word dante dactyl-mode cython-mode csv-mode cpp-auto-include counsel-projectile counsel-gtags counsel-css company-ycmd company-web company-rtags company-reftex company-math company-cabal company-c-headers company-auctex company-anaconda column-enforce-mode color-identifiers-mode cmm-mode clean-aindent-mode centered-cursor-mode blacken auto-yasnippet auto-highlight-symbol auto-dictionary auto-compile auctex-latexmk attrap aggressive-indent ace-link ac-ispell))
  ;;  '(pdf-view-midnight-colors '("#b2b2b2" . "#292b2e"))
  ;;  '(safe-local-variable-values
  ;;    '((c-file-offsets
  ;;       (innamespace . 0)
  ;;       (inline-open . 0)
  ;;       (case-label . +))
  ;;      (javascript-backend . tide)
  ;;      (javascript-backend . tern)
  ;;      (javascript-backend . lsp)))
  ;;  '(warning-suppress-log-types '((use-package) (emacsql) (emacsql) (emacsql)))
  ;;  '(warning-suppress-types '((use-package) (emacsql) (emacsql) (emacsql))))
  ;; (custom-set-faces
  ;;  ;; custom-set-faces was added by Custom.
  ;;  ;; If you edit it by hand, you could mess it up, so be careful.
  ;;  ;; Your init file should contain only one such instance.
  ;;  ;; If there is more than one, they won't work right.
  ;;  '(default ((t (:background "#140414" :foreground "#ffddff"))))
  ;;  '(cursor ((t (:background "#e6db74" :foreground: "#fff5f5"))))
  ;;  '(font-lock-builtin-face ((t (:foreground "#ff88bb"))))
  ;;  '(font-lock-constant-face ((t (:foreground "#992255"))))
  ;;  '(font-lock-function-name-face ((t (:weight normal :foreground "#cc99ff"))))
  ;;  '(font-lock-keyword-face ((t (:weight bold :foreground "#cc0060"))))
  ;;  '(font-lock-type-face ((t (:weight normal :foreground "#ddbbff"))))
  ;;  '(fringe ((t (:background "#140414" :foreground "#ffccee"))))
  ;;  '(highlight ((t (:background "#e6db74" :foreground "#500050"))))
  ;;  '(highlight-parentheses-highlight ((nil (:weight ultra-bold))) t)
  ;;  '(hl-line ((t (:background "#500050"))))
  ;;  '(lazy-highlight ((t (:background "#ff88bb" :foreground "#ffccee"))))
  ;;  '(linum ((t (:background "#140414" :foreground "#cc99ff"))))
  ;;  '(mode-line ((t (:background "#cc99ff" :foreground "#0a0a0a"))))
  ;;  '(mode-line-inactive ((t (:background "#1f0010" :foreground "#ffccee"))))
  ;;  '(popup-enu-selection-face ((t (:background "#00fff0" :foreground "#ff0000"))))
  ;;  '(popup-face ((t (:background "#500050" :foreground "#ffddff"))))
  ;;  '(popup-menu-face ((t (:background "#0000ff" :foreground "#00ff00"))))
  ;;  '(region ((t (:background "#ddbbff"))))
  ;;  '(show-paren-match ((t (:background "#992255" :foreground "#ffccee"))))
  ;;  '(spacemacs-insert-face ((t (:background "#cc0060" :foreground "#ffddff"))))
  ;;  '(spacemacs-normal-face ((t (:background "#1f0010" :foreground "#ffddff"))))
  ;;  '(vertical-border ((t (:background "#140414" :foreground "#ffccee")))))
  )
